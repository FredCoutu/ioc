﻿using System.Collections.Generic;
using DI.Class;

namespace DI.Interface.Repository
{
    public interface IRepositoryCar
    {
        List<ICar> GetListCar();

        void AddCar(Car car);
    }

    
}