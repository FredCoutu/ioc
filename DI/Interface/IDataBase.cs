﻿using System.Collections.Generic;
using DI.Class;

namespace DI.Interface
{
    public interface IDataBase
    {
       List<ICar> Read();

       void AddCar(Car car);
    }
}
