﻿using System.Collections.Generic;
using DI.Interface;
using DI.Interface.Repository;

namespace DI.Class.Repository
{
    public class RepositoryCar : IRepositoryCar
    {
        private IDataBase _database;

        public  RepositoryCar(IDataBase database)
        {
            _database = database;
        }
        
        public void AddCar(Car car)
        {
            car.Wheels  = 2;
            car.Window = 2;

            _database.AddCar(car);

        }

        public List<ICar> GetListCar()
        {
            return _database.Read();
        }
    }
}
