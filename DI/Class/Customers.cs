﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DI.Class.Repository;
using DI.Interface.Repository;

namespace DI.Class
{
    public class Customers
    {
        private int _money;
        private readonly IRepositoryCar _repositoryCar;
        private readonly List<ICar> _listCar = new List<ICar>();

        public Customers(IRepositoryCar repositoryCar)
        {
            _repositoryCar = repositoryCar;
        }

        public void CustomerBuyNewCar(Car car)
        {
            _repositoryCar.AddCar(car);
        }

        public bool CustomerIsHappy()
        {
            int goodCar = 0;

            _listCar.AddRange(_repositoryCar.GetListCar());

            _money = 1000;

            foreach (Car item in _listCar)
            {
                if (item.InGoodCondition())
                    goodCar++;

                _money = _money - 100;
            }

            if (goodCar < _listCar.Count())
                return false;

            if (_money > 500)
                return true;

            return false;
            
        }

    }
}
