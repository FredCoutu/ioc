﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DI.Class
{
    public class Car : ICar
    {
        public bool Color;

        public int Wheels;
        public int Window;
        public bool HadAccident;

        public virtual bool InGoodCondition()
        {
            if (Wheels == 4 && Window == 5)
                return true;
            return false;
        }
    }
}
    