﻿using System.Collections.Generic;
using DI.Interface;

namespace DI.Class.DB
{
    class XML : BaseDataBase, IDataBase
    {
        public override List<ICar> Read()
        {
            return _list;
        }

        public void  AddCar(Car car)
        {
            _list.Add(car);
        }
    }
}
