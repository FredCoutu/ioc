﻿using System.Collections.Generic;
using DI.Interface;

namespace DI.Class.DB
{
    public class Oracle : BaseDataBase, IDataBase
    {
        public override List<ICar> Read()
        {
            return _list;
        }

        public override void AddCar(Car car)
        {
            _list.Add(car);
        }
    }
}
