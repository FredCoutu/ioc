﻿using DI.Class;
using DI.Class.DB;
using DI.Class.Repository;
using System;
using System.Windows.Forms;

namespace DI
{
    public partial class Form1 : Form
    {
        private Customers customer;

        public Form1()
        {
            InitializeComponent();

            //option # 1
            // var container = new Container();
            // container.Options.AllowOverridingRegistrations = true;
            // container.Register<IHuman, Child>();
            // container.Register<IDb, Oracle>();

            //// container.Register<IDb, XML>();
            // container.Register<IRepositoryChild, RepositoryChild>();


            // Option #2

            
            var database = new Oracle();
            var repository = new RepositoryCar(database);
            customer = new Customers(repository);
        }

        private void btnHappyness_Click(object sender, EventArgs e)
        {
            var happy = customer.CustomerIsHappy();
            TxtIsHappy.Text = happy.ToString();
        }

        private void btnAudi_Click(object sender, EventArgs e)
        {

            var bmw = new Bmw();
            customer.CustomerBuyNewCar(bmw);

        }

        private void btnPorsche_Click(object sender, EventArgs e)
        {
            var porches = new Porches();
            customer.CustomerBuyNewCar(porches);
        }
    }
}
