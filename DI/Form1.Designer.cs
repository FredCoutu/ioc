﻿namespace DI
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BtnCustomerHappy = new System.Windows.Forms.Button();
            this.TxtIsHappy = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.BtnAudi = new System.Windows.Forms.Button();
            this.BtnPorsche = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // BtnCustomerHappy
            // 
            this.BtnCustomerHappy.Location = new System.Drawing.Point(12, 205);
            this.BtnCustomerHappy.Name = "BtnCustomerHappy";
            this.BtnCustomerHappy.Size = new System.Drawing.Size(119, 23);
            this.BtnCustomerHappy.TabIndex = 0;
            this.BtnCustomerHappy.Text = "Is Customer Happy";
            this.BtnCustomerHappy.UseVisualStyleBackColor = true;
            this.BtnCustomerHappy.Click += new System.EventHandler(this.btnHappyness_Click);
            // 
            // TxtIsHappy
            // 
            this.TxtIsHappy.Location = new System.Drawing.Point(158, 208);
            this.TxtIsHappy.Name = "TxtIsHappy";
            this.TxtIsHappy.Size = new System.Drawing.Size(100, 20);
            this.TxtIsHappy.TabIndex = 1;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(146, 26);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(100, 20);
            this.textBox2.TabIndex = 2;
            // 
            // BtnAudi
            // 
            this.BtnAudi.Location = new System.Drawing.Point(12, 95);
            this.BtnAudi.Name = "BtnAudi";
            this.BtnAudi.Size = new System.Drawing.Size(75, 23);
            this.BtnAudi.TabIndex = 3;
            this.BtnAudi.Text = "New Audi";
            this.BtnAudi.UseVisualStyleBackColor = true;
            this.BtnAudi.Click += new System.EventHandler(this.btnAudi_Click);
            // 
            // BtnPorsche
            // 
            this.BtnPorsche.Location = new System.Drawing.Point(12, 124);
            this.BtnPorsche.Name = "BtnPorsche";
            this.BtnPorsche.Size = new System.Drawing.Size(91, 23);
            this.BtnPorsche.TabIndex = 4;
            this.BtnPorsche.Text = "new Porsche";
            this.BtnPorsche.UseVisualStyleBackColor = true;
            this.BtnPorsche.Click += new System.EventHandler(this.btnPorsche_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.BtnPorsche);
            this.Controls.Add(this.BtnAudi);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.TxtIsHappy);
            this.Controls.Add(this.BtnCustomerHappy);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button BtnCustomerHappy;
        private System.Windows.Forms.TextBox TxtIsHappy;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Button BtnAudi;
        private System.Windows.Forms.Button BtnPorsche;
    }
}

