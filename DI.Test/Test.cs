﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SimpleInjector;
using DI.Class;
using DI.Interface;
using DI.Class.DB;
using DI.Interface.Repository;
using DI.Class.Repository;
using NSubstitute;
using System.Collections.Generic;

namespace DI.Test
{
    [TestClass]
    public class Test
    {

        public static void RegisterIOCUnitTest()
        {
            var container = new Container();
            container.Options.AllowOverridingRegistrations = true;
            container.Register<ICar, Car>();
            container.Register<IDataBase, Oracle>();
            container.Register<IRepositoryCar, RepositoryCar>();

        }

        [TestMethod]
        public void TestDadIsUnHappy()
        {
            // arrange
            RegisterIOCUnitTest();

            var mockRepositoryCar = Substitute.For<IRepositoryCar>();
            var database = new Oracle();
            //var repository = new RepositoryCar(database);

            var list = new List<ICar>();

            var porches = new Porches();
            porches.Wheels = 3;
            porches.Wheels = 1;

            list.Add(porches);
                    
            //act
            mockRepositoryCar.GetListCar().ReturnsForAnyArgs(list);
            var customer = new Customers(mockRepositoryCar);

            var happy = customer.CustomerIsHappy(); 
            //assert

            Assert.AreEqual(happy, true, "error test" );
        }
    }
}
